package ru.iteco.vetoshnikov.taskmanager.service;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.iteco.vetoshnikov.taskmanager.api.service.IDomainService;
import ru.iteco.vetoshnikov.taskmanager.api.service.IProjectService;
import ru.iteco.vetoshnikov.taskmanager.api.service.ITaskService;
import ru.iteco.vetoshnikov.taskmanager.api.service.IUserService;
import ru.iteco.vetoshnikov.taskmanager.dto.DomainDTO;
import ru.iteco.vetoshnikov.taskmanager.util.ConvertEndtityAndDTOUtil;

@Component
@Transactional
public  class DomainService implements IDomainService {
    @Autowired
    private IUserService userService;
    @Autowired
    private IProjectService projectService;
    @Autowired
    private ITaskService taskService;

    @Override
    public void save(@Nullable DomainDTO domain) {
        domain.setUserList(new ConvertEndtityAndDTOUtil().convertUserToDTOList(userService.findAll()));
        domain.setProjectList(new ConvertEndtityAndDTOUtil().convertProjectToDTOList(projectService.findAll()));
        domain.setTaskList(new ConvertEndtityAndDTOUtil().convertTaskToDTOList(taskService.findAll()));
    }

    @Override
    public void load(@Nullable DomainDTO domain)  {
        if (domain == null) return;
        userService.load(domain);
        projectService.load(domain);
        taskService.load(domain);
    }
}

