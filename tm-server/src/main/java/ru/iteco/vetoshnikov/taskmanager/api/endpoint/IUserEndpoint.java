package ru.iteco.vetoshnikov.taskmanager.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.dto.DomainDTO;
import ru.iteco.vetoshnikov.taskmanager.dto.UserDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IUserEndpoint {
    @WebMethod
    void createUserUser(
            @WebParam(name = "userObject") @Nullable UserDTO userObject
    );

    @WebMethod
    void mergeUser(
            @WebParam(name = "userObject") @Nullable UserDTO userObject
    );

    @WebMethod
    void removeUser(
            @WebParam(name = "userId") @Nullable String userId
    );

    @WebMethod
    void clearUser(
    );

    @WebMethod
    UserDTO findOneUser(
            @WebParam(name = "userName") @Nullable String userName
    );

    @WebMethod
    List<UserDTO> findAllUser(
    ) ;

    @WebMethod
    void loadUser(
            @WebParam(name = "domainObject") @Nullable DomainDTO domainObject
    );

    @WebMethod
    String getIdUserUser(
            @WebParam(name = "userName") @Nullable String userName
    );
}
