package ru.iteco.vetoshnikov.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.iteco.vetoshnikov.taskmanager.entity.Project;

import java.util.*;

@Repository
public interface ProjectRepository extends JpaRepository<Project, String> {
    @Query(value = "SELECT a FROM Project a WHERE user.id=:userId")
    List<Project> findAllByUserId(@Param("userId") @NotNull final String userId);

    @Modifying
    @Query(value = "DELETE FROM Project a WHERE a.user.id=:userId")
    void removeAllByUserId(@Param("userId") @NotNull final String userId);
}
