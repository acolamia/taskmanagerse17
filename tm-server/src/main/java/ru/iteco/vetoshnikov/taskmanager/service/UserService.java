package ru.iteco.vetoshnikov.taskmanager.service;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.iteco.vetoshnikov.taskmanager.api.service.IUserService;
import ru.iteco.vetoshnikov.taskmanager.dto.DomainDTO;
import ru.iteco.vetoshnikov.taskmanager.entity.User;
import ru.iteco.vetoshnikov.taskmanager.repository.UserRepository;
import ru.iteco.vetoshnikov.taskmanager.util.ConvertEndtityAndDTOUtil;

import java.util.List;

@Component
@Transactional
public class UserService implements IUserService {
    @Autowired
    private UserRepository userRepository;

    @Override
    public void createUser(@Nullable final User user) {
        if (user == null) return;
        userRepository.save(user);
    }

    @Override
    public void merge(@Nullable final User user) {
        if (user == null) return;
        userRepository.save(user);
    }

    @Override
    public List<User> merge(@Nullable final List<User> userList) {
        if (userList == null || userList.isEmpty()) return null;
        for (@Nullable final User user : userList) {
            if (user == null) continue;
            userRepository.save(user);
        }
        return userList;
    }

    @Override
    public void remove(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;
        userRepository.deleteById(userId);
    }

    @Override
    public User findOne(@Nullable final String userLogin) {
        if (userLogin == null || userLogin.isEmpty()) return null;
        List<User> userList = userRepository.findAll();
        for (User user : userList) {
            if (userLogin.equals(user.getLogin())) {
                return user;
            }
        }
        return null;
    }

    @Override
    public @Nullable
    final List<User> findAll() {
        List<User> userList = null;
        userList = userRepository.findAll();
        return userList;
    }

    @Override
    public void load(@Nullable final DomainDTO domain) {
        if (domain == null) return;
        userRepository.deleteAll();
        List<User> userList = new ConvertEndtityAndDTOUtil().convertDTOToUserList(domain.getUserList());
        userRepository.saveAll(userList);
    }

    @Override
    public String getIdUser(@Nullable final String login) {
        if (login == null || login.isEmpty()) return null;
        List<User> userList = userRepository.findAll();
        for (User user : userList) {
            if (login.equals(user.getLogin())) {
                return user.getId();
            }
        }
        return null;
    }

    @Override
    public void clear() {
        userRepository.deleteAll();
    }
}
