package ru.iteco.vetoshnikov.taskmanager.util;

import org.jetbrains.annotations.NotNull;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class HashUtil {
    public static String getHash(@NotNull final String md5) {
        String md5Hash=null;
        try {
            final MessageDigest md = MessageDigest.getInstance("MD5");
            final byte[] hashInBytes = md.digest(md5.getBytes(StandardCharsets.UTF_8));
            final StringBuilder sb = new StringBuilder();
            for (final byte b : hashInBytes) {
                sb.append(String.format("%02x", b));
            }
            md5Hash = sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return md5Hash;
    }
}
