package ru.iteco.vetoshnikov.taskmanager;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.iteco.vetoshnikov.taskmanager.bootstrap.Bootstrap;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.config.SpringConfig;

public class AppServer {

    public static void main(@Nullable String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);
        @NotNull Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.init();
    }
}