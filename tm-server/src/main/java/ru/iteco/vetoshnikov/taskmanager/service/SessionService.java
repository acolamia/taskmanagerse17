package ru.iteco.vetoshnikov.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.iteco.vetoshnikov.taskmanager.api.service.ISessionService;
import ru.iteco.vetoshnikov.taskmanager.api.service.IUserService;
import ru.iteco.vetoshnikov.taskmanager.entity.Session;
import ru.iteco.vetoshnikov.taskmanager.entity.User;
import ru.iteco.vetoshnikov.taskmanager.repository.SessionRepository;
import ru.iteco.vetoshnikov.taskmanager.util.HashUtil;

import java.util.List;

@Component
@Transactional
public class SessionService implements ISessionService {
    @Autowired
    private SessionRepository sessionRepository;
    @Autowired
    private IUserService userService;

    @Override
    public User checkPassword(
            @Nullable final String userLogin,
            @Nullable final String userPassword
    ) {
        if (userLogin == null || userLogin.isEmpty()) return null;
        if (userPassword == null || userPassword.isEmpty()) return null;
        @Nullable final List<User> userList = userService.findAll();
        for (User getUser : userList) {
            if (userLogin.equals(getUser.getLogin())) {
                @NotNull final User user = getUser;
                @Nullable final String passwordHash = HashUtil.getHash(userPassword);
                if (passwordHash == null) return null;
                if (passwordHash.equals(user.getPassword()))
                    return user;
            }
        }
        return null;
    }

    @Override
    public Session createSession(
            @Nullable final Session session
    ) {
        if (session == null) return null;
        sessionRepository.save(session);
        return session;
    }

    public void removeSession(
            @Nullable final Session session
    ) {
        if (session == null) return;
        sessionRepository.delete(session);
    }

    public Session findSession(
            @Nullable final Session session
    ) {
        if (session == null) return null;
        List<Session> sessionList = sessionRepository.findAll();
        for (Session getSession : sessionList) {
            if (getSession.getId().equals(session.getId()))
                return getSession;
        }
        return null;
    }
}
