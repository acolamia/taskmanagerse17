package ru.iteco.vetoshnikov.taskmanager.service;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.iteco.vetoshnikov.taskmanager.api.service.ITaskService;
import ru.iteco.vetoshnikov.taskmanager.dto.DomainDTO;
import ru.iteco.vetoshnikov.taskmanager.entity.Task;
import ru.iteco.vetoshnikov.taskmanager.repository.TaskRepository;
import ru.iteco.vetoshnikov.taskmanager.util.ConvertEndtityAndDTOUtil;

import java.util.List;

@Component
@Transactional
public class TaskService implements ITaskService {
    @Autowired
    private TaskRepository taskRepository;

    @Override
    public void createTask(@Nullable final Task task) {
        if (task == null || task.getProject().getId() == null || task.getProject().getId().isEmpty()) return;
        taskRepository.save(task);
    }

    @Override
    public void merge(@Nullable final Task task) {
        if (task == null) return;
        taskRepository.save(task);
    }

    @Override
    public List<Task> merge(@Nullable final List<Task> taskList) {
        if (taskList == null || taskList.isEmpty()) return null;
        for (@Nullable final Task task : taskList) {
            if (task == null) continue;
            taskRepository.save(task);
        }
        return taskList;
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskName) {
        if (userId == null || userId.isEmpty()) return;
        if (projectId == null || projectId.isEmpty()) return;
        if (taskName == null || taskName.isEmpty()) return;
        List<Task> taskList = taskRepository.findAllByProject(userId, projectId);
        for (Task task : taskList) {
            if (taskName.equals(task.getName())) {
                taskRepository.delete(task);
            }
        }
    }

    @Override
    public void removeAllByProject(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) return;
        if (projectId == null || projectId.isEmpty()) return;
        List<Task> taskList = taskRepository.findAllByProject(userId, projectId);
        for (Task task : taskList) {
            taskRepository.delete(task);
        }
    }

    @Override
    public void clear() {
        taskRepository.deleteAll();
    }

    @Override
    public Task findOne(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskName) {
        if (userId == null || userId.isEmpty()) return null;
        if (projectId == null || projectId.isEmpty()) return null;
        if (taskName == null || taskName.isEmpty()) return null;
        List<Task> taskList = taskRepository.findAllByProject(userId, projectId);
        for (Task task : taskList) {
            if (taskName.equals(task.getName())) {
                return task;
            }
        }
        return null;
    }

    @Override
    public @Nullable
    final List<Task> findAll() {
        List<Task> taskList = taskRepository.findAll();
        return taskList;
    }

    @Override
    public @Nullable
    final List<Task> findAllByProject(String userId, @Nullable final String projectId) {
        List<Task> taskList = taskRepository.findAllByProject(userId, projectId);
        return taskList;
    }

    @Override
    public void load(@Nullable final DomainDTO domain) {
        taskRepository.deleteAll();
        @Nullable final List<Task> taskList = new ConvertEndtityAndDTOUtil()
                .convertDTOToTaskList(domain.getTaskList());
        taskRepository.saveAll(taskList);
    }

    @Override
    public String getIdTask(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskName) {
        if (userId == null || userId.isEmpty()) return null;
        if (projectId == null || projectId.isEmpty()) return null;
        if (taskName == null || taskName.isEmpty()) return null;
        List<Task> taskList = taskRepository.findAllByProject(userId, projectId);
        for (Task task : taskList) {
            if (taskName.equals(task.getName())) {
                return task.getId();
            }
        }
        return null;
    }
}