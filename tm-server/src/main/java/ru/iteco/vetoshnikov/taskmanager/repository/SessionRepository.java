package ru.iteco.vetoshnikov.taskmanager.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.iteco.vetoshnikov.taskmanager.entity.Session;

@Repository
public interface SessionRepository extends JpaRepository<Session,String> {
}
