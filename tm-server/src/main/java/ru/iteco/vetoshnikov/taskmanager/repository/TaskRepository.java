package ru.iteco.vetoshnikov.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.iteco.vetoshnikov.taskmanager.entity.Task;

import java.util.*;

@Repository
public interface TaskRepository extends JpaRepository<Task, String> {
    @Query("SELECT a FROM Task a WHERE (user.id=:userId AND project.id=:projectId)")
    List<Task> findAllByProject(@Param("userId") @NotNull final String userId, @Param("projectId") @NotNull final String projectId);
}
