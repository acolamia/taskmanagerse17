package ru.iteco.vetoshnikov.taskmanager.command.task;

import ru.iteco.vetoshnikov.taskmanager.command.AbstractCommand;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public final class TaskClearCommand extends AbstractCommand {
    @Override
    public String command() {
        return "task-clear";
    }

    @Override
    public String description() {
        return "удаляет все задачи в веденном вами проекте.";
    }

    @Override
    public void execute() {
        serviceLocator.getTaskEndpointService().getTaskEndpointPort().clearTask();
    }
}
