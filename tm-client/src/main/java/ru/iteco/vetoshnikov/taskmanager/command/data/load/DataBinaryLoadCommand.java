package ru.iteco.vetoshnikov.taskmanager.command.data.load;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Session;
import ru.iteco.vetoshnikov.taskmanager.command.AbstractCommand;

@NoArgsConstructor
public class DataBinaryLoadCommand extends AbstractCommand {
    @Override
    public String command() {
        return "data-load-bin";
    }

    @Override
    public String description() {
        return "загружает *.bin файл в базу.";
    }

    @Override
    public void execute() {
        @NotNull final Session session = serviceLocator.getSessionStatusService().getSession();
        serviceLocator.getDomainEndpointService().getDomainEndpointPort().loadBinary(session);
        System.out.println("Загрузка в базу завершена, необходимо перезайти в учетную запись.");
    }
}
