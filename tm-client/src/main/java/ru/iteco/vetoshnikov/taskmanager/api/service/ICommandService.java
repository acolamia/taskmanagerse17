package ru.iteco.vetoshnikov.taskmanager.api.service;

public interface ICommandService {
    java.util.Map<String, ru.iteco.vetoshnikov.taskmanager.command.AbstractCommand> getCommandMap();
}
