package ru.iteco.vetoshnikov.taskmanager.api.customApi;

public interface ISessionStatusService {
    ru.iteco.vetoshnikov.taskmanager.api.endpoint.Session getSession();

    void setSession(ru.iteco.vetoshnikov.taskmanager.api.endpoint.Session session);
}
