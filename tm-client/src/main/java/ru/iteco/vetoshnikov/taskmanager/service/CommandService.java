package ru.iteco.vetoshnikov.taskmanager.service;

import org.springframework.stereotype.Service;
import ru.iteco.vetoshnikov.taskmanager.api.service.ICommandService;
import ru.iteco.vetoshnikov.taskmanager.command.AbstractCommand;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import java.util.LinkedHashMap;
import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
@Service
public class CommandService implements ICommandService {
    @NotNull public final Map<String, AbstractCommand> commandMap = new LinkedHashMap<>();
}
