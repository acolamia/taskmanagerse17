package ru.iteco.vetoshnikov.taskmanager.command.user;

import ru.iteco.vetoshnikov.taskmanager.command.AbstractCommand;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public final class UserLogoutCommand extends AbstractCommand {
    @Override
    public String command() {
        return "logout";
    }

    @Override
    public String description() {
        return "Завершение текущего сеанса пользователя.";
    }

    @Override
    public void execute() {
        serviceLocator.getSessionStatusService().setSession(null);
        System.out.println("Сеанс завершен.");
    }
}
