package ru.iteco.vetoshnikov.taskmanager.command;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.iteco.vetoshnikov.taskmanager.api.IService;
import ru.iteco.vetoshnikov.taskmanager.api.IServiceLocator;

import javax.xml.datatype.DatatypeConfigurationException;

@Getter
@Setter
@NoArgsConstructor
@Component
public abstract class AbstractCommand {
    @Autowired
    protected IServiceLocator serviceLocator;
    @Autowired
    protected IService service;
    protected boolean secure = true;

    public AbstractCommand(@NotNull IServiceLocator serviceLocator, @NotNull IService service) {
        this.serviceLocator = serviceLocator;
        this.service = service;
    }

    public abstract String command();

    public abstract String description();

    public abstract void execute() throws DatatypeConfigurationException;
}
