package ru.iteco.vetoshnikov.taskmanager.api.customApi;

import ru.iteco.vetoshnikov.taskmanager.api.endpoint.ITaskEndpoint;

import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceFeature;

public interface ITaskEndpointService {
    @WebEndpoint(name = "TaskEndpointPort")
    ITaskEndpoint getTaskEndpointPort();

    @WebEndpoint(name = "TaskEndpointPort")
    ITaskEndpoint getTaskEndpointPort(WebServiceFeature... features);
}
