package ru.iteco.vetoshnikov.taskmanager.api.customApi;

import ru.iteco.vetoshnikov.taskmanager.api.endpoint.IUserEndpoint;

import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceFeature;

public interface IUserEndpointService {
    @WebEndpoint(name = "UserEndpointPort")
    IUserEndpoint getUserEndpointPort();

    @WebEndpoint(name = "UserEndpointPort")
    IUserEndpoint getUserEndpointPort(WebServiceFeature... features);
}
