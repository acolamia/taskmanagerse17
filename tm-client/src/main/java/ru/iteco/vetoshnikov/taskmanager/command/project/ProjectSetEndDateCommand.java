package ru.iteco.vetoshnikov.taskmanager.command.project;

import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Project;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Session;
import ru.iteco.vetoshnikov.taskmanager.command.AbstractCommand;
import ru.iteco.vetoshnikov.taskmanager.util.DateUtil;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.Date;
import java.util.GregorianCalendar;

@NoArgsConstructor
public final class ProjectSetEndDateCommand extends AbstractCommand {
    @Override
    public @NotNull String command() {
        return "project-change-enddate";
    }

    @Override
    public @NotNull String description() {
        return "Изменяет время окончания проекта.";
    }

    @Override
    public void execute() throws DatatypeConfigurationException{
        @NotNull final Session session = serviceLocator.getSessionStatusService().getSession();
        System.out.println("Введите имя проекта для которого нужно изменить время конца: ");
        @Nullable final String projectName = service.getScanner().nextLine();
        System.out.println("Введите дату конца проекта в формате ДД.ММ.ГГГГ: ");
        @Nullable final String newEndDateString = service.getScanner().nextLine();
        @Nullable final Date newDate = DateUtil.parseDate(newEndDateString);
        @Nullable final Project project = serviceLocator.getProjectEndpointService().getProjectEndpointPort().findOneProject(session.getUserId(), projectName);

        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.setTime(newDate);
        XMLGregorianCalendar xmlGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar);
        project.setEndDate(xmlGregorianCalendar);

        project.setEndDate(xmlGregorianCalendar);
        serviceLocator.getProjectEndpointService().getProjectEndpointPort().mergeProject(project);
    }
}
