package ru.iteco.vetoshnikov.taskmanager.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.AbstractEntity;

import java.util.List;

public interface IRepository<E extends AbstractEntity> {
    void merge(@NotNull final E t);

    void remove(@NotNull final String key);

    void clear();

    E findOne(@NotNull final String key);

    @NotNull List<E> findAll();
}
