package ru.iteco.vetoshnikov.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Session;
import ru.iteco.vetoshnikov.taskmanager.command.AbstractCommand;

import javax.xml.datatype.DatatypeConfigurationException;

public class UserRemoveCommand extends AbstractCommand {
    @Override
    public String command() {
        return "user-remove";
    }

    @Override
    public String description() {
        return "Регистрация нового пользователя.";
    }

    @Override
    public void execute() throws DatatypeConfigurationException {
        @NotNull final Session session = serviceLocator.getSessionStatusService().getSession();
        System.out.print("Введите логин которого нужно удалить: ");
        String userName = service.getScanner().nextLine();
        if (userName == null || userName.isEmpty()) {
            System.out.println("Вы не ввели имя пользователя");
            return;
        }
        serviceLocator.getUserEndpointService().getUserEndpointPort().removeUser(userName);
        System.out.println("Пользователь с именем "+userName+" удален");
    }
}
