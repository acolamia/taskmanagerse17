package ru.iteco.vetoshnikov.taskmanager.endpoint;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;
import javax.xml.ws.Service;

import ru.iteco.vetoshnikov.taskmanager.api.customApi.ITaskEndpointService;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.ITaskEndpoint;

/**
 * This class was generated by Apache CXF 3.2.7
 * 2019-11-22T13:57:57.391+03:00
 * Generated source version: 3.2.7
 *
 */
@WebServiceClient(name = "TaskEndpointService",
                  wsdlLocation = "http://localhost:8080/TaskWebService?wsdl",
                  targetNamespace = "http://endpoint.taskmanager.vetoshnikov.iteco.ru/")
@org.springframework.stereotype.Service
public class TaskEndpointService extends Service implements ITaskEndpointService {

    public final static URL WSDL_LOCATION;

    public final static QName SERVICE = new QName("http://endpoint.taskmanager.vetoshnikov.iteco.ru/", "TaskEndpointService");
    public final static QName TaskEndpointPort = new QName("http://endpoint.taskmanager.vetoshnikov.iteco.ru/", "TaskEndpointPort");
    static {
        URL url = null;
        try {
            url = new URL("http://localhost:8080/TaskWebService?wsdl");
        } catch (MalformedURLException e) {
            java.util.logging.Logger.getLogger(TaskEndpointService.class.getName())
                .log(java.util.logging.Level.INFO,
                     "Can not initialize the default wsdl from {0}", "http://localhost:8080/TaskWebService?wsdl");
        }
        WSDL_LOCATION = url;
    }

    public TaskEndpointService(URL wsdlLocation) {
        super(wsdlLocation, SERVICE);
    }

    public TaskEndpointService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public TaskEndpointService() {
        super(WSDL_LOCATION, SERVICE);
    }

    public TaskEndpointService(WebServiceFeature ... features) {
        super(WSDL_LOCATION, SERVICE, features);
    }

    public TaskEndpointService(URL wsdlLocation, WebServiceFeature ... features) {
        super(wsdlLocation, SERVICE, features);
    }

    public TaskEndpointService(URL wsdlLocation, QName serviceName, WebServiceFeature ... features) {
        super(wsdlLocation, serviceName, features);
    }




    /**
     *
     * @return
     *     returns ITaskEndpoint
     */
    @Override
    @WebEndpoint(name = "TaskEndpointPort")
    public ITaskEndpoint getTaskEndpointPort() {
        return super.getPort(TaskEndpointPort, ITaskEndpoint.class);
    }

    /**
     *
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns ITaskEndpoint
     */
    @Override
    @WebEndpoint(name = "TaskEndpointPort")
    public ITaskEndpoint getTaskEndpointPort(WebServiceFeature... features) {
        return super.getPort(TaskEndpointPort, ITaskEndpoint.class, features);
    }

}
