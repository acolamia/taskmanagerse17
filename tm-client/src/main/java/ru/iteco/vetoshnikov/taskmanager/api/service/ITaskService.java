package ru.iteco.vetoshnikov.taskmanager.api.service;

import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Domain;
import org.jetbrains.annotations.NotNull;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Task;

import java.util.List;

public interface ITaskService {
    void createTask(@NotNull final Task task);

    void merge(@NotNull final Task task);

    List<Task> merge(@NotNull final List<Task> list);

    void remove(@NotNull final String key);

    void removeAllByProject(@NotNull final String projectId);

    void clear();

    String getIdTask(@NotNull final String userId, @NotNull final String projectId, @NotNull final String name);

    Task findOne(@NotNull final String key);

    @NotNull List<Task> findAll();

    void load(@NotNull final Domain domain);
}
